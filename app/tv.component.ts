import {Component, ChangeDetectionStrategy, OnInit} from 'angular2/core';
import {HTTP_PROVIDERS}    from 'angular2/http';
import {Control}    from 'angular2/common';
import { Router } from 'angular2/router';
import {OrderBy} from "./orderBy.pipe";
import {FilterBy} from "./filterBy";
import {Imdb} from './imdb';
import {CONSTANTS} from './constants';
import {ImdbService} from './imdb.service';
import {TVDetailComponent} from './tv-detail.component';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {SearchBoxDirective} from './searchBox.directive'
import {FilterWrapperDirective} from './filterWrapper.directive'
import {Multiselect, MultiSelectItem} from './multiselect.directive';

@Component({
	selector: 'tv-series',
	templateUrl: 'app/tv.component.html',
	styleUrls: ['app/tv.component.css'],
	directives: [TVDetailComponent, SearchBoxDirective, FilterWrapperDirective, Multiselect],
	providers: [HTTP_PROVIDERS, FilterBy],
	pipes: [OrderBy]
})
export class TVComponent implements OnInit {
	public title = 'Sit Back and Relax';
	public cacheTVSeries: Imdb[];
	public bufferTVSeries: Imdb[];
	public currentTVSeries: Imdb[];
	public selectedItem: Imdb;
	private lastString: string;
	public lastSortKey: string;
	public term = new Control();
	public ind = 0;
	public searchBox: string;
	public searchBoxClass: string = "glyphicon glyphicon-search";
	public imdbSliderValue: number = 5;
	public selectedLanguages;
	multiselectGenre: Array<any> = [];
	multiselectLanguage: Array<any> = [];
	genres: string = '[{"id": "1", "summary":"Action"}, {"id":"2", "summary":"Adventure"}, {"id": "3", "summary": "Comedy"}, {"id": "4",  "summary":"Drama"}, {"id": "5", "summary":"Horror"}, {"id": "6", "summary":"Romance"}]';
	languages: string = '[{"id": "1", "summary":"English"}, {"id":"2", "summary":"Hindi"}, {"id": "3", "summary": "Tamil"}]';
	genresList: Array<MultiSelectItem>;
	languagesList: Array<MultiSelectItem>;

	constructor(private _router: Router, private _imdbService: ImdbService, private _filter: FilterBy) {


		this.term.valueChanges
		.debounceTime(200)
		.distinctUntilChanged()
		.subscribe(term => 
		{
			this._filter.setTitle(term.toString());
			this.setTopTVSeries();
		})
		this._imdbService.setCallerClass(this);
		this._imdbService.setCallBackFunction(this.addDataToCurrentTVSeries);
	}

	clear(){
		this.currentTVSeries = [];
		this.cacheTVSeries = [];
		this.bufferTVSeries = [];
	}

	getTVSeries() {
		this._imdbService.getTVSeries();
	}

	addDataToCurrentTVSeries(chunks) {
		let start = 0;
		let check = JSON.parse(chunks[0]);

		if(check.sno == 0){
			start = 0;
			this.clear();
		}
		else{
			start = this.currentTVSeries.length;
		}

		for (var i = start; i < chunks.length - 1; ++i) {
			this.currentTVSeries.push(JSON.parse(chunks[i]));
			this.bufferTVSeries.push(JSON.parse(chunks[i]));
		}
		if (this.cacheTVSeries.length < CONSTANTS.DISPLAY_ITEMS_COUNT) {
			this.bufferTVSeries = this.bufferTVSeries.filter(this._filter.imdbFilter, this._filter);
			this.cacheTVSeries = this.bufferTVSeries.slice(0, CONSTANTS.DISPLAY_ITEMS_COUNT);
		}

		if(this.cacheTVSeries.length >= 8){
			this._imdbService.isLoadingFinished = true; 
		}

		if (chunks.length > CONSTANTS.BUFFER_SIZE) {
			this.lastString = null;
		}
	}

	isScrolledIntoView(el) {
		var elemTop = el.getBoundingClientRect().top;
		var elemBottom = el.getBoundingClientRect().bottom;

		var isVisible = (elemTop >= 0) && ((elemBottom+400) <= (window.innerHeight));
		return isVisible;
	}

	loadMore(){
		let start = this.cacheTVSeries.length - 1;
		if(this.cacheTVSeries.length < this.bufferTVSeries.length){
			let limit = Math.min((this.cacheTVSeries.length + 3), this.bufferTVSeries.length);
			// console.log(limit);
			this.cacheTVSeries = this.cacheTVSeries.concat(this.bufferTVSeries.slice(start, limit));

			var thumbnail = document.getElementsByClassName('thumbnail');
			for (var i = 0; i < thumbnail.length; ++i) {
				if(this.isScrolledIntoView(thumbnail[i])){
					thumbnail[i].style.visibility = "hidden";
				}
				else{
					thumbnail[i].style.visibility = "visible";
				}
			}
		}
	}

	onScroll(event) {
		let scroller = document.body;
		let offset = window.innerHeight;
		if (scroller.scrollTop + offset >= (scroller.scrollHeight) - 100) {
			this.loadMore();
		}
	}

	assignFromRequest(data){
		this.bufferTVSeries = data;
		this.currentTVSeries = data;
		this.cacheTVSeries = this.bufferTVSeries.slice(0, CONSTANTS.DISPLAY_ITEMS_COUNT);
		if (this.bufferTVSeries.length > CONSTANTS.BUFFER_SIZE) {
			this.bufferTVSeries = this.currentTVSeries.slice(0, CONSTANTS.BUFFER_SIZE);
		}
	}

	setTopTVSeries() {
		this.selectedItem = null;
		//Reset the imdb-detail if user starts typing
		// if buffer can hold and already searched string is extended
		if (this.lastString && (this.lastString.length >= 1) && this.lastString.length <= this._filter.getTitle().length && this._filter.getTitle().indexOf(this.lastString) !== -1) {
			this.bufferTVSeries = this.currentTVSeries.filter(this._filter.imdbFilter,this._filter);
			this.cacheTVSeries = this.bufferTVSeries.slice(0, CONSTANTS.DISPLAY_ITEMS_COUNT);
		}
		// Out of buffer size or new input
		else{
			this.lastString = this._filter.getTitle();
			this._imdbService.getTVSeriesByAttribute(CONSTANTS.TITLE,this._filter.getTitle());
		}
	}

	onSelect(imdb: Imdb) {
		console.log(imdb.title); 
		// this.selectedItem = imdb; 
		let link = ['TVDetail', { id: imdb.imdb_id }];
		  this._router.navigate(link);
	}

	independantSort(key,isAsc) {

		if(!key){
			this.bufferTVSeries = this.currentTVSeries.filter(this._filter.imdbFilter, this._filter);
			this.bufferTVSeries = this.bufferTVSeries.slice(0, CONSTANTS.BUFFER_SIZE);
			this.cacheTVSeries = this.bufferTVSeries.slice(0, CONSTANTS.DISPLAY_ITEMS_COUNT);
			this.lastSortKey = null;
			this._filter.setSortKey('');
			// console.log("resetting");
			return;
		}

		if (key == this._filter.getSortKey()) {
			return;
		}
		else{
			this.lastSortKey = key;
			this._filter.setSortKey(key);
		}

		if(this.currentTVSeries && this.currentTVSeries.length <= CONSTANTS.BUFFER_SIZE){
			this.bufferTVSeries = this._filter.sortByKey(this.bufferTVSeries, key, isAsc);
			this.bufferTVSeries = this.bufferTVSeries.filter(this._filter.imdbFilter, this._filter);
			this.cacheTVSeries = this.bufferTVSeries.slice(0, CONSTANTS.DISPLAY_ITEMS_COUNT);
		}
		else{
			this._imdbService.getTVSeriesByOrder(key, isAsc);
		}
	}

	//TODO: Move to directive
	onKeyChangeSearchIcon(searchText: string) {
		if(searchText.length > 0) {
			this.searchBoxClass = 'glyphicon glyphicon-remove';
		}
		else {
			this.searchBoxClass = 'glyphicon glyphicon-search';
		}
		// console.log(this.searchBoxClass); 
	}
	//TODO: Move to directive
	onClickResetSearch() {
		document.forms['searchForm'].reset();
		this.searchBoxClass = 'glyphicon glyphicon-search';
		this._filter.setTitle(null);
		this.getTVSeries();
	}

	updateModelGenre(multiselectModel: any) {
	    this.multiselectGenre = multiselectModel;
	    // console.log(multiselectModel);
	}

	updateModelLanguage(multiselectModel: any) {
	    this.multiselectLanguage = multiselectModel;
	    // console.log(multiselectModel);
	}

	ngOnInit() {
		
		this.getTVSeries();
	}
}