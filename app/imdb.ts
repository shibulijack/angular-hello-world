export interface Imdb {
  imdb_id: string;
  type: string;
  title: string;
  imdb_rating: string;
  genre: string[];
  language: string;
  date: string;
  year: string;
  plot: string;
  poster: string;
}