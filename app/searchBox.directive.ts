import {Directive, ElementRef, Renderer, Input} from 'angular2/core';

@Directive({
	selector: '.searchBox', 
	host: {
		'(focus)': 'onFocus()',
		'(blur)': 'onBlur()'
	}
})

export class SearchBoxDirective {
  constructor(private _elementRef: ElementRef, private _renderer: Renderer) {}

  onFocus() { this._setInputFocus(true); }
  onBlur() { this._setInputFocus(false); }

  private _setInputFocus(isSet: boolean) {
	  this._renderer.setElementClass(this._elementRef.nativeElement.parentElement, 'focus', isSet);
  }
}