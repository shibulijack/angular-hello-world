import {Injectable} from 'angular2/core';
import {Pipe, PipeTransform} from 'angular2/core';

@Pipe({
	name: 'filterBy',
	pure: false
})
@Injectable()
export class FilterBy {

	public static sortKey: string;
	public static imdb_id: string;
	public static title: string;
	public static genres: string[];
	public static languages: string[];
	public static date: string;
	public static year: string;
	public static range_start: number;
	public static range_end: number;
	public static sortStyle: boolean;

	constructor() {
		if(!this.getSortKey()){
			this.setSortKey('year');
			this.setSortStyle(false);
		}
		this.setRangeEnd(10);
	}

	getObject(){
		let tempObject = new Object();
		tempObject.title = this.getTitle();
		tempObject.genres = this.getGenres();
		tempObject.languages = this.getLanguages();
		tempObject.range_start = this.getRangeStart();
		tempObject.range_end = this.getRangeEnd();
		tempObject.sortKey = this.getSortKey();
		tempObject.sortStyle = this.getSortStyle();
		return tempObject;
	}
	
	getTitle() {
		return FilterBy.title;
	}

	getSortKey() {
		return FilterBy.sortKey;
	}

	getGenres() {
		return FilterBy.genres;
	}

	getLanguages() {
		return FilterBy.languages;
	}

	getRangeStart() {
		return FilterBy.range_start;
	}

	getRangeEnd() {
		return FilterBy.range_end;
	}

	getSortStyle(){
		return FilterBy.sortStyle;
	}

	setTitle(title) {
		FilterBy.title = title;
	}

	setSortKey(sortKey) {
		FilterBy.sortKey = sortKey;
	}

	setGenres(genres) {
		FilterBy.genres = genres;
	}

	setLanguages(languages) {
		FilterBy.languages = languages;
	}

	setRangeStart(range_start:number) {
		FilterBy.range_start = range_start;
	}

	setRangeEnd(range_end:number) {
		FilterBy.range_end = range_end;
	}

	setSortStyle(style:boolean){
		FilterBy.sortStyle = style;
	}

	clear(){
		this.setGenres(null);
		this.setLanguages(null);
		this.setTitle(null);
		this.setRangeStart(null);
		this.setRangeEnd(10);
		this.setSortKey('year');
	}


	imdbRatingFilter(movie: any, range_start: number, range_end: number): boolean{
		if((!range_start)){
			return true;
		}
		if(movie.rating >= range_start && movie.rating <= range_end){
			return true;
		}
		return false;
	}

	imdbNameFilter(movie:any, title: string): boolean{
		if (!title)
			return true;

		title = title.toLocaleLowerCase();

		if(movie.title.toLocaleLowerCase().indexOf(title) !== -1){
			return true;
		}

		return false;
	}

	imdbLanguageFilter(movie: any, languages: string[]): boolean{
		if(languages) {

			for(let l in languages){
				if (l == movie.language)
					return true;
			}
			return false;
		}
		return true;
	}

	imdbGenreFilter(movie: any, genres: string[]): boolean {
		if (genres && genres.length>0) {
			for (let g in genres) {
				if (movie.genre.indexOf(genres[g]) !== -1)
					return true;
			}
			return false;
		}
		return true;
	}


	imdbFilter(movie: any): any{

		return this.imdbNameFilter(movie, this.getTitle()) && this.imdbRatingFilter(movie, this.getRangeStart(), this.getRangeEnd()) 
		&& this.imdbLanguageFilter(movie, this.getLanguages()) && this.imdbGenreFilter(movie,this.getGenres());
	}

	sortByKey(array, key, order) {
		return array.sort(function(a, b) {
			var x = a[key];
			var y = b[key];

			if (key == 'date') {
				x = x.split("-").join();
				y = y.split("-").join();
				if(x == 0){
					x = a['year'] * Math.pow(10, 4);
				}
				if(y == 0){
					y = b['year'] * Math.pow(10, 4);
				}
			}
			else {
				if (typeof x == "string") {
					x = x.toLowerCase();
					y = y.toLowerCase();
				}
			}

			if (order)
				return ((x < y) ? -1 : ((x > y) ? 1 : 0));
			else
				return ((x > y) ? -1 : ((x < y) ? 1 : 0));
		});
	}
}