import {Component, ChangeDetectionStrategy, OnInit} from 'angular2/core';
import {HTTP_PROVIDERS}    from 'angular2/http';
import {Control}    from 'angular2/common';
import { Router } from 'angular2/router';
import {OrderBy} from "./orderBy.pipe";
import {FilterBy} from "./filterBy";
import {Imdb} from './imdb';
import {CONSTANTS} from './constants';
import {ImdbService} from './imdb.service';
import {MovieDetailComponent} from './movie-detail.component';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {SearchBoxDirective} from './searchBox.directive'
import {FilterWrapperDirective} from './filterWrapper.directive'
import {Multiselect, MultiSelectItem} from './multiselect.directive';

@Component({
	selector: 'movies',
	templateUrl: 'app/movies.component.html',
	styleUrls: ['app/movies.component.css'],
	directives: [MovieDetailComponent, SearchBoxDirective, FilterWrapperDirective, Multiselect],
	providers: [HTTP_PROVIDERS, FilterBy],
	pipes: [OrderBy]
})
export class MoviesComponent implements OnInit {
	public title = 'Sit Back and Relax';
	public movies: Observable<Imdb[]>;
	public cacheMovies: Imdb[];
	public bufferMovies: Imdb[];
	public currentMovies: Imdb[];
	public selectedItem: Imdb;
	private lastString: string;
	public lastSortKey: string;
	public term = new Control();
	public ind = 0;
	public searchBox: string;
	public searchBoxClass: string = "glyphicon glyphicon-search";
	public imdbSliderValue: number = 5;
	public selectedLanguages;
	multiselectGenre: Array<any> = [];
	multiselectLanguage: Array<any> = [];
	genres: string = '[{"id": "1", "summary":"Action"}, {"id":"2", "summary":"Adventure"}, {"id": "3", "summary": "Comedy"}, {"id": "4",  "summary":"Drama"}, {"id": "5", "summary":"Horror"}, {"id": "6", "summary":"Romance"}]';
	languages: string = '[{"id": "1", "summary":"English"}, {"id":"2", "summary":"Hindi"}, {"id": "3", "summary": "Tamil"}]';
	genresList: Array<MultiSelectItem>;
	languagesList: Array<MultiSelectItem>;

	constructor(private _router: Router, private _imdbService: ImdbService, private _filter: FilterBy) {

		const storedGenreItems = <Array<any>>JSON.parse(this.genres);
		this.genresList = storedGenreItems.map(i => new MultiSelectItem(i));
		const storedLanguageItems = <Array<any>>JSON.parse(this.languages);
		this.languagesList = storedLanguageItems.map(i => new MultiSelectItem(i));

		this.term.valueChanges
		.debounceTime(200)
		.distinctUntilChanged()
		.subscribe(term => 
		{
			this._filter.setTitle(term.toString());
			this.setTopMovies();
		})
		this._imdbService.setCallerClass(this);
		this._imdbService.setCallBackFunction(this.addDataToCurrentMovies);
	}

	clear(){
		this.currentMovies = [];
		this.cacheMovies = [];
		this.bufferMovies = [];
	}

	getMovies() {
		this._imdbService.getMovies();
	}

	addDataToCurrentMovies(chunks) {
		let start = 0;
		
		let check = JSON.parse(chunks[0]);

		if(check['sno'] == 0){
			start = 0;
			this.clear();
		}
		else{
			start = this.currentMovies.length;
		}

		for (var i = start; i < chunks.length - 1; ++i) {
			this.currentMovies.push(JSON.parse(chunks[i]));
			this.bufferMovies.push(JSON.parse(chunks[i]));
		}
		if (this.cacheMovies.length < CONSTANTS.DISPLAY_ITEMS_COUNT) {
			console.log(chunks);
			this.bufferMovies = this.bufferMovies.filter(this._filter.imdbFilter, this._filter);
			this.cacheMovies = this.bufferMovies.slice(0, CONSTANTS.DISPLAY_ITEMS_COUNT);
		}

		if(this.cacheMovies.length >= 8){
			this._imdbService.isLoadingFinished = true; 
		}

		if (chunks.length > CONSTANTS.BUFFER_SIZE) {
			this.lastString = null;
		}
	}

	isScrolledIntoView(el) {
		var elemTop = el.getBoundingClientRect().top;
		var elemBottom = el.getBoundingClientRect().bottom;

		var isVisible = (elemTop >= 0) && ((elemBottom+400) <= (window.innerHeight));
		return isVisible;
	}

	loadMore(){
		let start = this.cacheMovies.length - 1;
		if(this.cacheMovies.length < this.bufferMovies.length){
			let limit = Math.min((this.cacheMovies.length + 3), this.bufferMovies.length);
			// console.log(limit);
			this.cacheMovies = this.cacheMovies.concat(this.bufferMovies.slice(start, limit));

			var thumbnail = document.getElementsByClassName('thumbnail');
			for (var i = 0; i < thumbnail.length; ++i) {
				if(this.isScrolledIntoView(thumbnail[i])){
					thumbnail[i].style.visibility = "hidden";
				}
				else{
					thumbnail[i].style.visibility = "visible";
				}
			}
		}
	}

	onScroll(event) {
		let scroller = document.body;
		let offset = window.innerHeight;
		if (scroller.scrollTop + offset >= (scroller.scrollHeight) - 100) {
			this.loadMore();
		}
	}

	assignFromRequest(data){
		this.bufferMovies = data;
		this.currentMovies = data;
		this.cacheMovies = this.bufferMovies.slice(0, CONSTANTS.DISPLAY_ITEMS_COUNT);
		if (this.bufferMovies.length > CONSTANTS.BUFFER_SIZE) {
			this.bufferMovies = this.currentMovies.slice(0, CONSTANTS.BUFFER_SIZE);
		}
	}

	setTopMovies() {
		this.selectedItem = null;
		//Reset the imdb-detail if user starts typing
		// if buffer can hold and already searched string is extended
		if (this.lastString && (this.lastString.length >= 1) && this.lastString.length <= this._filter.getTitle().length && this._filter.getTitle().indexOf(this.lastString) !== -1) {
			this.bufferMovies = this.currentMovies.filter(this._filter.imdbFilter,this._filter);
			this.cacheMovies = this.bufferMovies.slice(0, CONSTANTS.DISPLAY_ITEMS_COUNT);
		}
		// Out of buffer size or new input
		else{
			this.lastString = this._filter.getTitle();
			this._imdbService.getMovies();
		}
	}

	onSelect(imdb: Imdb) {
		console.log(imdb.title); 
		// this.selectedItem = imdb; 
		let link = ['MovieDetail', { id: imdb.imdb_id }];
		  this._router.navigate(link);
	}

	filterOut(){
		this._filter.setRangeStart(this.imdbSliderValue);
		console.log(this._filter.getObject());
		if(this.currentMovies.length < CONSTANTS.BUFFER_SIZE){
			this.bufferMovies = this.currentMovies.filter(this._filter.imdbFilter, this._filter);
			this.cacheMovies = this.bufferMovies.slice(0, CONSTANTS.DISPLAY_ITEMS_COUNT);
		}
		else{
			this._imdbService.getMovies();
		}
	}

	independantSort(key,isAsc) {

		if(!key){
			this.bufferMovies = this.currentMovies.filter(this._filter.imdbFilter, this._filter);
			this.bufferMovies = this.bufferMovies.slice(0, CONSTANTS.BUFFER_SIZE);
			this.cacheMovies = this.bufferMovies.slice(0, CONSTANTS.DISPLAY_ITEMS_COUNT);
			this.lastSortKey = 'year';
			this._filter.setSortKey('year');
			// console.log("resetting");
			return;
		}

		if (key == this._filter.getSortKey()) {
			return;
		}
		else{
			this.lastSortKey = key;
			this._filter.setSortKey(key);
			this._filter.setSortStyle(isAsc);
		}

		if(this.currentMovies && this.currentMovies.length <= CONSTANTS.BUFFER_SIZE){
			this.bufferMovies = this._filter.sortByKey(this.bufferMovies, key, isAsc);
			this.bufferMovies = this.bufferMovies.filter(this._filter.imdbFilter, this._filter);
			this.cacheMovies = this.bufferMovies.slice(0, CONSTANTS.DISPLAY_ITEMS_COUNT);
		}
		else{
			this._imdbService.getMovies();
		}
	}

	//TODO: Move to directive
	onKeyChangeSearchIcon(searchText: string) {
		if(searchText.length > 0) {
			this.searchBoxClass = 'glyphicon glyphicon-remove';
		}
		else {
			this.searchBoxClass = 'glyphicon glyphicon-search';
		}
		// console.log(this.searchBoxClass); 
	}
	//TODO: Move to directive
	onClickResetSearch() {
		document.forms['searchForm'].reset();
		this.searchBoxClass = 'glyphicon glyphicon-search';
		this._filter.setTitle(null);
		this.getMovies();
	}

	updateModelGenre(multiselectModel: any) {

		this._filter.setGenres(this.getArrayOfSelectedValues(multiselectModel));
	}

	updateModelLanguage(multiselectModel: any) {

		this._filter.setLanguages(this.getArrayOfSelectedValues(multiselectModel));
	}

	getArrayOfSelectedValues(obj){
		let combine = new Array();
		for (var i = obj.length - 1; i >= 0; i--) {
			combine.push(obj[i].summary);
		}
		return combine;
	}

	ngOnInit(){	
		this.getMovies();
	}
}