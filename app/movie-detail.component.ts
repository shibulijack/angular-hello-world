import {Component, Input, OnInit} from 'angular2/core';
import { RouteParams } from 'angular2/router';
import { Router } from 'angular2/router';
import {Imdb} from './imdb';
import { ImdbService } from './imdb.service';
import {FormatTime} from './formatTime.pipe'
import {VgPlayer, VgControls, VgPlayPause, VgPlaybackButton, VgScrubBar, VgScrubBarCurrentTime, VgScrubBarBufferingTime, VgMute, VgFullscreen, VgOverlayPlay} from 'videogular2/videogular2';

@Component({
	selector: 'movie-detail',
	templateUrl: 'app/movie-detail.component.html',
	styleUrls: ['app/movie-detail.component.css'],
	directives: [
        VgPlayer,
        VgOverlayPlay,
        VgControls,
        VgPlayPause,
        VgPlaybackButton,
        VgScrubBar,
        VgScrubBarCurrentTime,
        VgScrubBarBufferingTime,
        VgMute,
        VgFullscreen
    ],
    pipes: [FormatTime]
})
export class MovieDetailComponent implements OnInit {
	@Input() imdb: Imdb;
	sources: Array<Object>;
	constructor(
		private _imdbService: ImdbService,
		private _routeParams: RouteParams,
		private _router: Router) {

		this._imdbService.setCallerClass(this);
		this._imdbService.setCallBackFunction(this.addCurrentMovieInfo);

		this.sources = [
		    {
			src: "http://iurevych.github.com/Flat-UI-videos/big_buck_bunny.mp4",
		        type: "video/mp4"
		    }
		    // {
		    //     src: "http://static.videogular.com/assets/videos/videogular.ogg",
		    //     type: "video/ogg"
		    // },
		    // {
		    //     src: "http://static.videogular.com/assets/videos/videogular.webm",
		    //     type: "video/webm"
		    // }
		];
	}
	addCurrentMovieInfo(data){
		let info = JSON.parse(data[0]);
		this.imdb = info;
		console.log(this.imdb);
	}

	ngOnInit() {
		let id = this._routeParams.get('id');
		console.log("Routing to:" + id);
		this._imdbService.getMovieById('00Ori6yBN3');	//TODO
	}
	goBack() {
		let link = ['Movies']
		this._router.navigate(link);
	}

}
