import {Component} from 'angular2/core';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';
import {Imdb} from './imdb';
import {CONSTANTS} from './constants';
import {ImdbService} from './imdb.service';
import {MoviesComponent} from './movies.component'
import {TVComponent} from './tv.component'
import {MovieDetailComponent} from './movie-detail.component'
import {TVDetailComponent} from './tv-detail.component';

@Component({
	selector: 'my-app',
	templateUrl: 'app/app.component.html',
	// styleUrls: ['app/app.component.css'],
	directives: [ROUTER_DIRECTIVES],
	providers: [ROUTER_PROVIDERS, ImdbService]
})
@RouteConfig([
	{
		path: '/movies',
		name: 'Movies',
		component: MoviesComponent,
		useAsDefault: true
	},
	{
		path: '/movie/detail/:id',
		name: 'MovieDetail',
		component: MovieDetailComponent
	},
	{
		path: '/tv',
		name: 'TV',
		component: TVComponent,
	},
	{
		path: '/tv/detail/:id',
		name: 'TVDetail',
		component: TVDetailComponent
	},
])
export class AppComponent {
	public title = 'Sit Back and Relax';
	public isSideBarClosed: Boolean = true;
	public sideBarCollapseClass = "glyphicon glyphicon-menu-right";

	onSideBarClick() {
		this.isSideBarClosed = !this.isSideBarClosed
		if (this.isSideBarClosed) {
			this.sideBarCollapseClass = "glyphicon glyphicon-menu-right";
		}
		else {
			this.sideBarCollapseClass = "glyphicon glyphicon-menu-left";
		}
	}
}