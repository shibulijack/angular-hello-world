import {Directive, ElementRef, Input} from 'angular2/core';

@Directive({
	selector: '.filter-form'
})

export class FilterWrapperDirective {
  constructor(private _elementRef: ElementRef) {
	  _elementRef.nativeElement.style.height = window.innerHeight - 58;
  }
}