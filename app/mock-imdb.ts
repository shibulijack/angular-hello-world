import {Imdb} from './imdb'; 
export var MOVIES: Imdb[] = [
    {
        "imdb_id": "tt0011565",
        "type": "movie",
        "title": "The Penalty",
        "imdb_rating": "7.5",
        "genre": ["Crime", "Drama", "Thriller"],
        "language": "",
        "date": "0000-00-00",
        "year": "1920",
        "plot": "A deformed criminal mastermind plans to loot the city of San Francisco as well as revenge himself on the doctor who mistakenly amputated his legs.",
        "poster": ""
    }
];