import {Component, Input, OnInit} from 'angular2/core';
import { RouteParams } from 'angular2/router';
import {Imdb} from './imdb';
import { ImdbService } from './imdb.service';

@Component({
	selector: 'tv-detail',
	templateUrl: 'app/tv-detail.component.html',
	styleUrls: ['app/tv-detail.component.css']
})
export class TVDetailComponent implements OnInit {
	@Input() imdb: Imdb;
	constructor(
		private _imdbService: ImdbService,
		private _routeParams: RouteParams) {

		this._imdbService.setCallerClass(this);
		this._imdbService.setCallBackFunction(this.addCurrentMovieInfo);
	}
	addCurrentMovieInfo(data){
		let info = JSON.parse(data[0]);
		this.imdb = info;
	}

	ngOnInit() {
		let id = this._routeParams.get('id');
		console.log("Routing to:" + id);
	  this._imdbService.getTVSeriesById(id);	//TODO
	}
	goBack() {
		window.history.back();
	}

}
