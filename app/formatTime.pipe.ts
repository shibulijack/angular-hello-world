import {Pipe, PipeTransform} from 'angular2/core';
/*
 * Formats Milliseconds to HH:mm:ss
 * Usage:
 *   value | formatTime
 * Example:
 *   {{ 86400 |  formatTime}}
 *   formats to: 1024
*/
@Pipe({ name: 'formatTime' })
export class FormatTime implements PipeTransform {
  transform(value: number): string {
    var input = value / 1000;
    function z(n) { return (n < 10 ? '0' : '') + n; }
    var seconds = Math.round(input % 60);
    var minutes = Math.floor(input % 3600 / 60);
    var hours = Math.floor(input / 3600);
    return (z(hours) + ':' + z(minutes) + ':' + z(seconds));
  }
}