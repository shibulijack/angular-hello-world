<?php

require_once('./DBManager/DBManager.php');
require_once('./default_functions.php');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

}
header('Content-Type: application/json');

$db = new DBManager();

$url = $_GET['url'];
$parts = explode("/", $url);

$id = @$_POST['imdb_id'];
$title = @$_POST['title'];

if(isset($url) && !empty($parts)){
	switch ($parts[0]) {
		case 'imdb':
		$sql = constructGetQueryFromIMDB($parts);
		$result = ($db->executeQuery($sql));
		$t = array();
		foreach ($result as $key => $value) {
			$value['genre'] = explode(",", $value['genre']);
			$t[] =  $value;
		}
		echo json_encode($t);
		break;

		case 'link':
		if(authenticated()){
			if(isset($id)){

			}
			elseif (isset($title)) {

			}
		}
		break;

		default:
		# code...
		break;
	}
}

?>