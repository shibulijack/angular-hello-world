<?php

require_once(dirname(__FILE__).'/dbconstants.php');

/**
* 
*/
class DBManager
{
	
	public function __construct()
	{
		# code...
		try {
			$this->conn = new PDO("mysql:host=".SERVER_NAME.";dbname=".DB_NAME, USER_NAME, PASSWORD);
		    // set the PDO error mode to exception
			$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    //echo "Connected successfully";
		}
		catch(PDOException $e)
		{
			echo "Connection failed: " . $e->getMessage();
		}
	}

	public function getConnection(){
		return $this->conn;
	}

	public function addImdbDetails($id,$type,$title,$rating,$genre,$language,$date,$year,$plot,$poster){
		$stmt = $this->conn->prepare("INSERT INTO `imdb_details` VALUES ('$id','$type' ,'$title' ,'$rating','$genre','$language','$date','$year','$plot','$poster')");

		try{
			$stmt->execute();
		}
		catch(PDOException $e)
		{
			echo "Failed to execute: " . $e->getMessage();
		}
	}

	public function addTvDetails($id,$title,$url,$size,$season,$episode,$quality,$year){
		$stmt = $this->conn->prepare("INSERT INTO `tv_links` VALUES ('$id' ,'$title', '$url', '$size' ,'$season', '$episode', '$quality', '$year')");

		try{
			$stmt->execute();
		}
		catch(PDOException $e)
		{
			echo "Failed to execute: " . $e->getMessage();
		}
	}

	public function addMovieDetails($id,$title,$url,$size,$quality,$year){
		$stmt = $this->conn->prepare("INSERT INTO `movie_links` VALUES ('$id' ,'$title', '$url', '$size', '$quality', '$year')");

		try{
			$stmt->execute();
		}
		catch(PDOException $e)
		{
			echo "Failed to execute: " . $e->getMessage();
		}
	}

	public function getNearestMatch($name,$type){
		$stmt = $this->conn->prepare("SELECT * FROM `imdb_details` WHERE `title` LIKE '%$name%' and `type` = '$type' LIMIT 0 , 5");

		try{

			$stmt->execute();
			$stmt->setFetchMode(PDO::FETCH_ASSOC);
			$rows = $stmt->fetchAll();
			return $rows;
		}
		catch(PDOException $e)
		{
			
		}
		return null;
	}

	public function getAllMovies(){
		$stmt = $this->conn->prepare("SELECT * FROM `imdb_details` WHERE `type` = 'movie'");

		try{

			$stmt->execute();
			$stmt->setFetchMode(PDO::FETCH_ASSOC);
			$rows = $stmt->fetchAll();
			return $rows;
		}
		catch(PDOException $e)
		{
			
		}
		return null;
	}

	public function executeQuery($query){
		$stmt = $this->conn->prepare($query);

		try{

			$stmt->execute();
			$stmt->setFetchMode(PDO::FETCH_ASSOC);
			$rows = $stmt->fetchAll();
			return $rows;
		}
		catch(PDOException $e)
		{
			
		}
		return null;
	}
}

?>