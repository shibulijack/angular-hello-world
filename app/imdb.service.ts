import {Injectable} from 'angular2/core';
import {Imdb} from './imdb';
import {MOVIES} from './mock-imdb';
import {Observable}     from 'rxjs/Observable';
import {FilterBy} from "./filterBy.pipe";
@Injectable()
export class ImdbService {


	public isLoadingFinished = true;
	public xhttp;
	public movies;
	private postURL = 'http://shibulijack.com/server';

	private callerClass;
	private callBackFunction;

	constructor() {
		this.xhttp = new XMLHttpRequest();
		this.xhttp.responseType = "arrayBuffer";
		this.movies = new Array();
	}

	setCallerClass(caller: any){
		this.callerClass = caller;
	}

	setCallBackFunction(callBack: any){
		this.callBackFunction = callBack;
	}

	xhrPostLoad(url: string, payload  = true) {
		this.isLoadingFinished = false;
		if (this.xhttp && this.xhttp.readyState != 4) {
            this.xhttp.abort();
        }
		this.xhttp.open("POST", this.postURL + url);
		this.xhttp.addEventListener("progress", this.updateProgress.bind(null, this, this.callBackFunction, this.callerClass));
		this.xhttp.addEventListener("load", this.loadDone.bind(null, this));
		if(payload){
			this.xhttp.send(JSON.stringify(this.callerClass._filter.getObject()));
		}
		else{
			this.xhttp.send();
		}
	}
	
	xhrGetLoad(url: string) {
		this.isLoadingFinished = false;
		if (this.xhttp && this.xhttp.readyState != 4) {
            this.xhttp.abort();
        }
		this.xhttp.open("GET", this.postURL + url);
		this.xhttp.addEventListener("progress", this.updateProgress.bind(null, this, this.callBackFunction, this.callerClass));
		this.xhttp.addEventListener("load", this.loadDone.bind(null, this));
		this.xhttp.send();
	}

	loadDone(services) {
		services.isLoadingFinished = true;
	}

	updateProgress(services, callBackFunction, callerClass) {
		var pe = services.xhttp.response;
		var res = pe.split("\n");
		callBackFunction.apply(callerClass, [res]);
		// services.isLoadingFinished = true;
	}

	getMovies() {
		this.xhrPostLoad('/movie');
	}

	getMovieById(movieId: string) {
		this.xhrPostLoad('/movie/'+movieId,false);
	}

	getTVSeries() {
		this.xhrGetLoad('');
	}

	getTVSeriesById(movieId: string) {
		this.xhrPostLoad('');
	}

	getTVSeriesByAttribute(attribute, searchFilter) {
		this.xhrPostLoad('');
	}

	getTVSeriesByOrder(attribute,order) {
		if (!order)
			attribute = '-' + attribute;
		let url = this.apiTVSeriesURL + 'order/' + attribute;
		this.xhrLoad(url);
	}
}
